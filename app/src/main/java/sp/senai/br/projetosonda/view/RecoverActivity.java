package sp.senai.br.projetosonda.view;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.model.Funcionario;
import sp.senai.br.projetosonda.tasks.HandlerTask;
import sp.senai.br.projetosonda.tasks.HandlerTaskAdapter;
import sp.senai.br.projetosonda.tasks.TaskRest;
import sp.senai.br.projetosonda.util.JsonParser;

import static sp.senai.br.projetosonda.view.SuperClassActivity.IP;

/**
 * Created by Sonda on 18/04/2017.
 */

public class RecoverActivity extends NewPasswordActivity {
    private String urlRecuperar = "http://" + IP + "/GestaoDeTreinamento/funcionario/novaSenha";

    private TextView txtEmail, txtTitulo, txtDescricao;
    private View linha_1, linha_2;
    private Button btnLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover);

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        toolbar.setTitle(R.string.redefinir);
        setSupportActionBar(toolbar);

        // Adicionar botão voltar na ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Obter as dimensões da tela
        atribuirDimensoes();

        txtEmail = (TextView) findViewById(R.id.txtEmailRecover);
        txtDescricao = (TextView) findViewById(R.id.txtDescricao);
        txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        btnLogin = (Button) findViewById(R.id.btn_login);
        linha_1 = findViewById(R.id.view_linha_1);
        linha_2 = findViewById(R.id.view_linha_2);

        // Mudar fonte
        txtDescricao.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
        txtEmail.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
        btnLogin.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
        txtTitulo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));

        adaptarConteudo();
    }

    // Métodos de requisição

    // Modificar senha do funcionário
    public void mudarSenha(View view) {
        if (txtEmail.getEditableText().toString().trim().isEmpty()) {
            Toast.makeText(RecoverActivity.this, getResources().getString(R.string.vazio), Toast.LENGTH_SHORT).show();
        } else {
            Funcionario funcionario = new Funcionario();
            funcionario.setEmail(txtEmail.getEditableText().toString());
            JsonParser<Funcionario> parser = new JsonParser<>(Funcionario.class);
            new TaskRest(TaskRest.RequestMethod.POST, handlerTask, RecoverActivity.this).execute(urlRecuperar, parser.fromObject(funcionario));
        }
    }

    // Validar o e-mail inserido
    private HandlerTask handlerTask = new HandlerTaskAdapter() {
        @Override
        public void onError(Exception erro) {
            Log.e("ERRO", erro.getMessage());

            if (erro.getMessage().contains("403")) {
                Toast.makeText(getBaseContext(), getResources().getString(R.string.erro_email), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getBaseContext(), getResources().getString(R.string.erro_conexao), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccess(String retorno) {
            // Ir para próxima tela
            Intent intent = new Intent(getBaseContext(), NewPasswordActivity.class);
            startActivity(intent);

            // Mostrar animação
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
    };

    // Métodos da classe

    // Adaptar conteúdo à tela
    private void adaptarConteudo() {
        if ((larguraTela <= 480 && alturaTela <= 800) || (larguraTela <= 800 && alturaTela <= 480)) {
            txtTitulo.setTextSize(35);

            txtDescricao.setTextSize(20);
            txtEmail.setTextSize(20);
            btnLogin.setTextSize(17);
        } else if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
            // Alterar tamanho do texto
            txtTitulo.setTextSize(35);
            txtTitulo.setTextSize(txtTitulo.getTextSize() + (txtTitulo.getTextSize() / 250));
            txtTitulo.setPadding(0, 0, 0, 65);

            txtDescricao.setTextSize(17);
            txtDescricao.setTextSize(txtDescricao.getTextSize() + (txtDescricao.getTextSize() / 250));

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(85, 45, 85, 35);

            txtEmail.setLayoutParams(params);
            txtEmail.setTextSize(17);
            txtEmail.setTextSize(txtEmail.getTextSize() + (txtEmail.getTextSize() / 250));

            // Redimencionar a linha do título
            LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParam.height = 5;
            layoutParam.setMargins(85, 45, 85, 45);
            linha_1.setLayoutParams(layoutParam);
            linha_2.setLayoutParams(layoutParam);

            // Alterar tamanho do botão
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 65, 0, 0);
            btnLogin.setLayoutParams(layoutParams);

            btnLogin.setTextSize(15);
            btnLogin.setTextSize(btnLogin.getTextSize() + (btnLogin.getTextSize() / 250));
            btnLogin.setHeight(btnLogin.getHeight() + 150);
            btnLogin.setWidth(btnLogin.getWidth() + 350);
        }
    }

    // Finalizar activity quando clicar em voltar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                // Apresentar animação de passagem de direita para esquerda
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Finalizar a activity quando clicar em voltar do celular
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Finalizar activity
        finish();

        // Apresentar animação de passagem de direita para esquerda
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
