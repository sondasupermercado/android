package sp.senai.br.projetosonda.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.adapter.ProgramaAdapter;
import sp.senai.br.projetosonda.model.Aplicacao;
import sp.senai.br.projetosonda.model.Aprovado;
import sp.senai.br.projetosonda.model.Programa;
import sp.senai.br.projetosonda.util.RestUtil;

/**
 * Created by Sonda on 31/03/2017.
 */

public class HistoricoActivity extends SuperClassActivity {
    private ImageView imgSearch;
    private TextView txtVazio;
    private ListView listView;

    private ProgramaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Buscar a cor do tema
        String cor = buscarTema();
        if (cor.equals("azul")) {
            setTheme(R.style.AppTheme);
        } else {
            setTheme(R.style.AlternativeAppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        toolbar.setTitle(R.string.historico);

        // Atualizar a cor do tema
        atualizarTema(cor, toolbar);

        setSupportActionBar(toolbar);

        // Código do funcionário
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        id_funcionario = prefs.getLong(ID, 1);

        // Adicionar botão voltar na ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        imgSearch = (ImageView) findViewById(R.id.img_search);
        txtVazio = (TextView) findViewById(R.id.txtVisivel);
        listView = (ListView) findViewById(R.id.list_view);

        // Buscar programas já concluídos
        new BuscarHistorico().execute();

        // Obter as dimensões da tela
        atribuirDimensoes();

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // Verificar se o primeiro elemento visível é o primeiro elemento da listview
                int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();

                // Permitir que a lista atualize
                swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

            }
        });

        // Realizar ação ao recarregar a tela
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new BuscarHistorico().execute();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    // Métodos de requisição

    // Buscar programas já concluídos
    public class BuscarHistorico extends AsyncTask<Void, Void, String> {
        private List<Programa> programas = new ArrayList<>(), historico = new ArrayList<>();
        private List<Aplicacao> aplicacoes = new ArrayList<>();
        private ProgressDialog progresso;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progresso = new ProgressDialog(HistoricoActivity.this);
            progresso.setTitle(getResources().getString(R.string.aguarde));
            progresso.setMessage(getResources().getString(R.string.consultando));
            progresso.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String retorno = null;

            try {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                String token = String.valueOf(prefs.getString(TOKEN, null));

                retorno = RestUtil.get(urlAplicacoes + id_funcionario + "/funcionario", HistoricoActivity.this, token);
            } catch (IOException | RuntimeException erro) {
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (retorno != null && !retorno.equals("")) {
                    JSONArray jsonArray = new JSONArray(retorno);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject aplicacaoJson = jsonArray.getJSONObject(i);
                        Aplicacao aplicacao = form.fromJson(aplicacaoJson.toString(), Aplicacao.class);
                        boolean repetido = false;

                        // Adicionar Programas que o funcionário participa
                        if (aplicacao.getTreinamento() != null) {
                            if (programas.size() > 0) {
                                for (int j = 0; j < programas.size(); j++) {
                                    if (programas.get(j).getId() == aplicacao.getTreinamento().getPrograma().getId()) {
                                        repetido = true;
                                    }
                                }
                                if (!repetido) {
                                    programas.add(aplicacao.getTreinamento().getPrograma());
                                }
                            } else {
                                programas.add(aplicacao.getTreinamento().getPrograma());
                            }
                            aplicacoes.add(aplicacao);
                        }
                    }

                    // Verificar o status de cada treinamento
                    for (int i = 0; i < programas.size(); i++) {
                        int pendentes = 0, reprovados = 0;

                        Programa programa = programas.get(i);
                        for (int j = 0; j < aplicacoes.size(); j++) {
                            Aplicacao aplicacao = aplicacoes.get(j);

                            if (aplicacao.getTreinamento().getPrograma().getId() == programa.getId()) {
                                if (aplicacao.getAprovado().equals(Aprovado.PENDENTE)) {
                                    pendentes++;
                                } else if (aplicacao.getAprovado().equals(Aprovado.REPROVADO)) {
                                    reprovados++;
                                }
                            }
                        }

                        // Verificar se participa do programa
                        if (verificarPrograma(programa.getId()) != null) {
                            // Verificar se concluiu o programa
                            if (pendentes == 0) {
                                if (reprovados != 0) {
                                    programa.setAprovado(Aprovado.REPROVADO);
                                } else {
                                    programa.setAprovado(Aprovado.APROVADO);
                                }
                                historico.add(programa);
                            }
                        }
                    }
                } else {
                    if (!contemErros) {
                        contemErros = true;
                        erros = "Dados não encontrados";
                    }
                }

            } catch (JSONException | RuntimeException erro) {
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progresso.dismiss();

            if (erros == null) {
                if (!historico.isEmpty()) {
                    imgSearch.setVisibility(View.GONE);
                    txtVazio.setVisibility(View.GONE);

                    adapter = new ProgramaAdapter(HistoricoActivity.this, historico);

                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Programa programa = (Programa) parent.getItemAtPosition(position);
                            verPrograma(programa);
                        }
                    });

                    listView.setVisibility(View.VISIBLE);
                } else {
                    imgSearch.setVisibility(View.VISIBLE);
                    txtVazio.setVisibility(View.VISIBLE);

                    // Adaptar texto de lista vazia
                    if (alturaTela >= 2464 || larguraTela >= 2464) {
                        txtVazio.setTextSize(35);
                        txtVazio = aumentaTexto(txtVazio);

                        // Redimencionar imagem
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.height = (250);
                        layoutParams.width = (250);
                        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;

                        imgSearch.setLayoutParams(layoutParams);
                    } else if (larguraTela > 480 && alturaTela > 800) {
                        txtVazio.setTextSize(40);
                    }

                    txtVazio.setText(getResources().getString(R.string.lista_vazia));
                    txtVazio.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));

                    listView.setVisibility(View.GONE);
                }
                invalidateOptionsMenu();
            } else {
                tratarErro(HistoricoActivity.this);
            }
        }
    }

    // Métodos da classe

    // Inflar barra de pesquisa
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (adapter != null) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_search, menu);
            MenuItem menuItem = menu.findItem(R.id.search);

            SearchView searchView = (SearchView) menuItem.getActionView();

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);


                    return false;
                }
            });
        }

        return true;
    }

    // Ver informações do Programa
    private void verPrograma(Programa programa) {
        Intent resultado = new Intent(this, ProgramaView.class);
        resultado.putExtra("id_programa", programa.getId());
        startActivity(resultado);

        // Apresentar animação de passagem da esquerda para direita
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    // Finalizar activity quando clicar em voltar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Finalizar activity
                finish();

                // Apresentar animação de passagem de direita para esquerda
                startActivity(new Intent(HistoricoActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Quando voltar à tela de histórico
    @Override
    protected void onResume() {
        super.onResume();

        // Buscar os programas concluídos
        new BuscarHistorico().execute();
    }

    // Finalizar a activity quando clicar em voltar do celular
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Finalizar activity
        finish();

        // Apresentar animação de passagem de direita para esquerda
        startActivity(new Intent(HistoricoActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}