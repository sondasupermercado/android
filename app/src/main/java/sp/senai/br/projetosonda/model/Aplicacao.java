package sp.senai.br.projetosonda.model;

/**
 * Created by Sonda on 03/04/2017.
 */

public class Aplicacao {
    private Long id;
    private boolean reciclagem;
    private Funcionario funcionario;
    private Aprovado aprovado = Aprovado.PENDENTE;
    private byte nota;
    private Treinamento treinamento;
    private String dataInicio;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isReciclagem() {
        return reciclagem;
    }

    public void setReciclagem(boolean reciclagem) {
        this.reciclagem = reciclagem;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Aprovado getAprovado() {
        return aprovado;
    }

    public void setAprovado(Aprovado aprovado) {
        this.aprovado = aprovado;
    }

    public byte getNota() {
        return nota;
    }

    public void setNota(byte nota) {
        this.nota = nota;
    }

    public Treinamento getTreinamento() {
        return treinamento;
    }

    public void setTreinamento(Treinamento treinamento) {
        this.treinamento = treinamento;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    @Override
    public String toString() {
        return "Aplicacao{" +
                "id=" + id +
                ", aprovado=" + aprovado +
                ", programa=" + treinamento.getPrograma().getNome() +
                '}';
    }
}
