package sp.senai.br.projetosonda.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.model.Aplicacao;
import sp.senai.br.projetosonda.model.Aprovado;
import sp.senai.br.projetosonda.model.Programa;
import sp.senai.br.projetosonda.util.RestUtil;

/**
 * Created by Sonda on 31/03/2017.
 */

public class GraficoActivity extends SuperClassActivity {
    private TextView txtTitulo, txtVazio;
    private ImageView imgSearch;
    private PieChart pieChart;
    private View viewLinha;

    private String partes[] = new String[3];
    private int desempenhos[] = new int[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Buscar a cor do tema
        String cor = buscarTema();
        if (cor.equals("azul")) {
            setTheme(R.style.AppTheme);
        } else {
            setTheme(R.style.AlternativeAppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico);

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        toolbar.setTitle(R.string.grafico);

        // Atualizar a cor do tema
        atualizarTema(cor, toolbar);

        setSupportActionBar(toolbar);

        // Código do funcionário
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        id_funcionario = prefs.getLong(ID, 1);

        // Adicionar botão voltar na ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        imgSearch = (ImageView) findViewById(R.id.img_search);
        txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        txtVazio = (TextView) findViewById(R.id.txtVisivel);
        pieChart = (PieChart) findViewById(R.id.chart);
        viewLinha = findViewById(R.id.view_linha);

        partes[0] = getResources().getString(R.string.aprovado);
        partes[1] = getResources().getString(R.string.pendente);
        partes[2] = getResources().getString(R.string.reprovado);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);

        // Buscar as informações para o gráfico
        new PopularGrafico().execute();

        // Obter as dimensões da tela
        atribuirDimensoes();

        // Realizar ação ao recarregar a tela
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new PopularGrafico().execute();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    // Métodos de requisição

    // Buscar as informações para o gráfico
    private class PopularGrafico extends AsyncTask<Void, Void, String> {
        private List<Aplicacao> aplicacoes = new ArrayList<>();
        private List<Programa> programas = new ArrayList<>();
        private ProgressDialog progresso;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progresso = new ProgressDialog(GraficoActivity.this);
            progresso.setTitle(getResources().getString(R.string.aguarde));
            progresso.setMessage(getResources().getString(R.string.consultando));
            progresso.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String retorno = null;

            try {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                String token = String.valueOf(prefs.getString(TOKEN, null));

                retorno = RestUtil.get(urlAplicacoes + id_funcionario + "/funcionario", GraficoActivity.this, token);
            } catch (IOException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (retorno != null && !retorno.equals("")) {
                    JSONArray jsonArray = new JSONArray(retorno);

                    // Inicializar as quantidades
                    desempenhos[0] = 0;
                    desempenhos[1] = 0;
                    desempenhos[2] = 0;

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject aplicacaoJson = jsonArray.getJSONObject(i);
                        Aplicacao aplicacao = form.fromJson(aplicacaoJson.toString(), Aplicacao.class);
                        boolean repetido = false;

                        // Adicionar Programas que o funcionário participa
                        if (aplicacao.getTreinamento() != null) {
                            if (programas.size() > 0) {
                                for (int j = 0; j < programas.size(); j++) {
                                    if (programas.get(j).getId() == aplicacao.getTreinamento().getPrograma().getId()) {
                                        repetido = true;
                                    }
                                }
                                if (!repetido) {
                                    programas.add(aplicacao.getTreinamento().getPrograma());
                                }
                            } else {
                                programas.add(aplicacao.getTreinamento().getPrograma());
                            }

                            aplicacoes.add(aplicacao);

                        }
                    }

                    // Verificar o status de cada treinamento
                    for (int i = 0; i < programas.size(); i++) {
                        int pendentes = 0, reprovados = 0;

                        Programa programa = programas.get(i);
                        for (int j = 0; j < aplicacoes.size(); j++) {
                            Aplicacao aplicacao = aplicacoes.get(j);

                            if (aplicacao.getTreinamento().getPrograma().getId() == programa.getId()) {
                                if (aplicacao.getAprovado().equals(Aprovado.PENDENTE)) {
                                    pendentes++;
                                } else if (aplicacao.getAprovado().equals(Aprovado.REPROVADO)) {
                                    reprovados++;
                                }
                            }
                        }

                        // Verificar se participa do programa
                        if (verificarPrograma(programa.getId()) != null) {
                            // Verificar se concluiu o programa
                            if (pendentes != 0) {
                                desempenhos[1] += 1;
                            } else if (reprovados != 0) {
                                desempenhos[2] += 1;
                            } else {
                                desempenhos[0] += 1;
                            }
                        }
                    }
                } else {
                    contemErros = true;
                    erros = "Dados não encontrados";
                }

            } catch (JSONException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progresso.dismiss();

            if (erros == null) {
                if (desempenhos[0] > 0 || desempenhos[1] > 0 || desempenhos[2] > 0) {
                    imgSearch.setVisibility(View.GONE);
                    txtVazio.setVisibility(View.GONE);

                    txtTitulo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));

                    // Adaptar título do gráfico
                    if (alturaTela <= 800 || larguraTela <= 800) {
                        txtTitulo.setTextSize(25);
                    } else if (alturaTela >= 2464 || larguraTela >= 2464) {
                        txtTitulo.setTextSize(30);
                        txtTitulo = aumentaTexto(txtTitulo);

                        // Redimencionar a linha do título
                        LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParam.height = 5;
                        layoutParam.setMargins(25, 40, 25, 40);
                        viewLinha.setLayoutParams(layoutParam);
                    }

                    pieChart.setVisibility(View.VISIBLE);
                    txtTitulo.setVisibility(View.VISIBLE);
                    viewLinha.setVisibility(View.VISIBLE);

                    gerarGrafico();
                } else {
                    imgSearch.setVisibility(View.VISIBLE);
                    txtVazio.setVisibility(View.VISIBLE);

                    // Adaptar texto de lista vazia
                    if (alturaTela >= 2464 || larguraTela >= 2464) {
                        txtVazio.setTextSize(35);
                        txtVazio = aumentaTexto(txtVazio);

                        // Redimencionar imagem
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.height = (250);
                        layoutParams.width = (250);
                        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;

                        imgSearch.setLayoutParams(layoutParams);
                    } else if (larguraTela > 480 && alturaTela > 800) {
                        txtVazio.setTextSize(40);
                    }

                    txtVazio.setText(getResources().getString(R.string.lista_vazia));
                    txtVazio.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));

                    pieChart.setVisibility(View.GONE);
                    txtTitulo.setVisibility(View.GONE);
                    viewLinha.setVisibility(View.GONE);
                }
            } else {
                tratarErro(GraficoActivity.this);
            }
        }
    }

    // Métodos da classe

    // Gerar o gráfico
    private void gerarGrafico() {
        // Lista com os valores e nomes das partes do gráfico
        List<PieEntry> pieEntries = new ArrayList<>();

        for (int i = 0; i < partes.length; i++) {
            if (desempenhos[i] != 0) {
                pieEntries.add(new PieEntry((float) desempenhos[i], partes[i]));
            }
        }

        ArrayList<Integer> colors = new ArrayList<>();
        if (desempenhos[0] > 0) {
            colors.add(ColorTemplate.rgb("#7FCA58")); // Aprovado #77BD52, #87cefa
        }
        if (desempenhos[1] > 0) {
            colors.add(ColorTemplate.rgb("#FBE355")); // Pendente
        }
        if (desempenhos[2] > 0) {
            colors.add(ColorTemplate.rgb("#F08080")); // Reprovado
        }

        // Enviar dados para o gráfico
        PieDataSet dataSet = new PieDataSet(pieEntries, "");
        dataSet.setColors(colors);

        final PieData dados = new PieData(dataSet);

        dados.setValueTextSize(20);
        dados.setValueTextColor(Color.BLACK);
        dados.setValueFormatter(new PercentFormatter());
        dados.setDrawValues(true);

        // Montando o gráfico
        final PieChart chart = (PieChart) findViewById(R.id.chart);
        chart.setData(dados);

        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setDrawEntryLabels(false);
        chart.setDrawHoleEnabled(false);
        chart.setTouchEnabled(true);
        chart.setRotationEnabled(false);
        chart.animateX(1000);

        // Quando clicar no gráfico
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (chart.isUsePercentValuesEnabled()) {
                    // Mostrar valores em quantidade
                    chart.setUsePercentValues(false);
                    dados.setValueFormatter(new DefaultValueFormatter(0));
                } else {
                    // Mostrar valores em porcentagem
                    chart.setUsePercentValues(true);
                    dados.setValueFormatter(new PercentFormatter());
                }
            }

            @Override
            public void onNothingSelected() {
                // Se nada está selecionado, não faz nada
            }
        });

        // Posicionar legenda do gráfico
        Legend l = chart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setTextColor(Color.WHITE);
        l.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

        // Adaptar texto à tela
        if (larguraTela <= 480 && alturaTela <= 800) {
            l.setTextSize(15);
        } else if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
            dados.setValueTextSize(40);
            l.setTextSize(40);
        } else {
            l.setTextSize(20);
        }

        Log.e("SIZE", l.getTextSize() + "");

        chart.invalidate();
    }

    // Finalizar activity quando clicar em voltar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Finalizar activity
                finish();

                // Apresentar animação de passagem de direita para esquerda
                startActivity(new Intent(GraficoActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Quando voltar à tela de gráfico
    @Override
    protected void onResume() {
        super.onResume();

        // Recarregar o gráfico
        new PopularGrafico().execute();
    }

    // Finalizar a activity quando clicar em voltar do celular
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Finalizar activity
        finish();

        // Apresentar animação de passagem de direita para esquerda
        startActivity(new Intent(GraficoActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}