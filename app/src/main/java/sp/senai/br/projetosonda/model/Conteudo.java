package sp.senai.br.projetosonda.model;

/**
 * Created by Sonda on 03/04/2017.
 */

public class Conteudo {
    private Long id;
    private String nome;
    private boolean ativo = true;
    private String objetivo;
    private int cargaHoraria;
    private byte notaDeCorte;
    private boolean presencial = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public byte getNotaDeCorte() {
        return notaDeCorte;
    }

    public void setNotaDeCorte(byte notaDeCorte) {
        this.notaDeCorte = notaDeCorte;
    }

    public boolean isPresencial() {
        return presencial;
    }

    public void setPresencial(boolean presencial) {
        this.presencial = presencial;
    }
}
