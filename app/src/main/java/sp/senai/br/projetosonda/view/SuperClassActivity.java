package sp.senai.br.projetosonda.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.model.Programa;
import sp.senai.br.projetosonda.util.RestUtil;

/**
 * Created by Sonda on 23/05/2017.
 */

public class SuperClassActivity extends AppCompatActivity {
    public final static String APP_PREFS = "app_prefs", EMAIL = "email", ID = "id", LOGADO = "usuario", SENHA = "senha", THEME = "tema", TOKEN = "token";
    protected final String urlAplicacoes = "http://" + IP + "/GestaoDeTreinamento/aplicacao/";
    protected String urlPrograma = "http://" + IP + "/GestaoDeTreinamento/programa/";
    public final static String IP = "192.168.0.101:8082";

    protected SwipeRefreshLayout swipeRefreshLayout;
    protected Toolbar toolbar;

    private List<Programa> programasFuncionario = new ArrayList<>();
    protected int larguraTela, alturaTela;
    protected static int tentativas = 0;
    protected Long id_funcionario;
    protected boolean contemErros;
    protected String erros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // Obter as dimensões da tela
    public void atribuirDimensoes() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        larguraTela = size.x;
        alturaTela = size.y;
    }

    // Buscar a cor do tema
    public String buscarTema() {
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        String cor = prefs.getString(THEME, "azul");
        return cor;
    }

    // Atualizar a cor do tema
    public void atualizarTema(String cor, Toolbar barraStatus) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.view_tela);

        switch (cor) {
            case "azul":
                // Mudar cor do tema
                this.setTheme(R.style.AppTheme);

                // Mudar cor da barra de status
                barraStatus.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                // Mudar cor de fundo
                linearLayout.setBackgroundColor(getResources().getColor(R.color.azulLogo));

                break;
            case "roxo":
                // Mudar cor do tema
                this.setTheme(R.style.AlternativeAppTheme);

                // Mudar cor da barra de status
                barraStatus.setBackgroundColor(getResources().getColor(R.color.colorAlternative));

                // Mudar cor de fundo
                linearLayout.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
                break;
        }
    }

    // Verificar se o funcionário está no programa
    public Programa verificarPrograma(Long id) {
        String retorno = "";

        if (programasFuncionario.size() == 0) {
            try {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                String token = String.valueOf(prefs.getString(TOKEN, null));
                id_funcionario = prefs.getLong(ID, 1);

                retorno = RestUtil.get(urlPrograma + "/participa/" + id_funcionario, this, token);
            } catch (IOException | RuntimeException erro) {
                erro.printStackTrace();
            }

            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (retorno != null && !retorno.equals("")) {
                    JSONArray jsonArray = new JSONArray(retorno);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject pJson = jsonArray.getJSONObject(i);
                        Programa programa = form.fromJson(pJson.toString(), Programa.class);

                        programasFuncionario.add(programa);
                    }
                }

            } catch (JSONException | RuntimeException erro) {
                erro.printStackTrace();
            }
        } else {
            for (int i = 0; i < programasFuncionario.size(); i++) {
                Programa programa = programasFuncionario.get(i);
                if (programa.getId() == id) {
                    return programa;
                }
            }
        }

        return null;
    }

    // Adaptar texto à tela
    public void adaptarTexto(TextView textView) {
        if (alturaTela <= 800 && larguraTela <= 480) {
            textView.setTextSize(20);
        }
    }

    public TextView aumentaTexto(TextView textView) {
        float tamanho = textView.getTextSize();
        textView.setTextSize(tamanho + (tamanho / 250));
        return textView;
    }

    // ---------------------------------------------------ERROS -----------------------------------------------------------------------

    // Tratar erro que ocorreu na requisição
    public void tratarErro(final Context context) {
        Log.e("Erro", erros);
        if (erros.contains("565")) {
            // Refazer Login
            Intent resultado = new Intent(context, LoginActivity.class);
            resultado.putExtra("renovar", true);
            startActivity(resultado);
            finish();

            // Não mostrar animação
            overridePendingTransition(0, 0);
        } else if (erros.equals("Dados não encontrados")) {
            buscarDados(context);
        } else if (erros.equals("Sessão Expirada")) {
            invalidarLogin(context);
        } else {
            final AlertDialog.Builder alertas = new AlertDialog.Builder(context);
            View dialoglayout = getLayoutInflater().inflate(R.layout.alert_error, null);

            TextView txtTitulo = (TextView) dialoglayout.findViewById(R.id.txtTitulo);
            TextView txtVerificar = (TextView) dialoglayout.findViewById(R.id.txtVerificar);
            TextView txtMensagem = (TextView) dialoglayout.findViewById(R.id.txtMensagem);

            if (tentativas > 3) {
                tentativas = 0;
            }

            // Adaptar texto da janela
            if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
                txtTitulo.setTextSize(20);
                txtVerificar.setTextSize(12);
                txtMensagem.setTextSize(15);

                txtTitulo = aumentaTexto(txtTitulo);
                txtVerificar = aumentaTexto(txtVerificar);
                txtMensagem = aumentaTexto(txtMensagem);

                // Aumentar tamanho do ícone da janela
                Drawable drawable = getResources().getDrawable(R.mipmap.ic_error_outline_white_24dp);
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                int tamanho = ((int) txtTitulo.getTextSize());
                drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));

                txtTitulo.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
            }

            adaptarTexto(txtTitulo);

            if (tentativas >= 3) {
                txtVerificar.setVisibility(View.VISIBLE);
            } else {
                txtVerificar.setVisibility(View.GONE);
            }

            txtTitulo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Medium.ttf"));
            txtVerificar.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
            txtMensagem.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

            alertas.setView(dialoglayout);

            alertas.setPositiveButton(getResources().getString(R.string.sim), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    tentativas++;

                    // Recarregar a tela
                    dialog.dismiss();
                    Intent intent = getIntent();
                    startActivity(intent);
                    finish();

                    // Não mostrar animação
                    overridePendingTransition(0, 0);
                }
            });

            alertas.setNegativeButton(getResources().getString(R.string.nao), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    tentativas = 0;

                    // Fechar caixa de alerta
                    dialog.dismiss();

                    if (context.getClass().equals(MainActivity.class)) {
                        // Minimizar aplicação
                        moveTaskToBack(true);
                    } else {
                        // Finalizar activity
                        finish();

                        // Apresentar animação de passagem da direita para esquerda
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    }
                }
            });

            alertas.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    tentativas++;

                    // Recarregar a tela
                    Intent intent = getIntent();
                    startActivity(intent);
                    finish();

                    // Não mostrar animação
                    overridePendingTransition(0, 0);
                    dialog.dismiss();
                }
            });

            alertas.show();
        }
    }

    // Exibir mensagem de dados não encontrados
    public void buscarDados(final Context context) {
        final AlertDialog.Builder alertas = new AlertDialog.Builder(context);
        View dialoglayout = getLayoutInflater().inflate(R.layout.alert_dados, null);

        TextView txtTitulo = (TextView) dialoglayout.findViewById(R.id.txtTitulo);
        TextView txtAviso = (TextView) dialoglayout.findViewById(R.id.txtMensagem);

        adaptarTexto(txtTitulo);

        txtTitulo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Medium.ttf"));
        txtAviso.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

        // Adaptar texto da janela
        if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
            txtTitulo.setTextSize(15);
            txtAviso.setTextSize(15);

            txtTitulo = aumentaTexto(txtTitulo);
            txtAviso = aumentaTexto(txtAviso);

            // Aumentar tamanho do ícone da janela
            Drawable drawable = getResources().getDrawable(R.mipmap.ic_error_outline_white_24dp);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            int tamanho = ((int) txtTitulo.getTextSize());
            drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));

            txtTitulo.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        }

        alertas.setView(dialoglayout);

        alertas.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);

                // Voltar ao tema padrão
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(THEME, "azul");
                editor.putString(EMAIL, null);
                editor.putString(SENHA, null);
                editor.commit();

                Intent intent = new Intent(context, LoginActivity.class);
                intent.putExtra("saiu", true);
                startActivity(intent);
                finish();

                // Apresentar animação de passagem da direita para esquerda
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });


        if (!context.getClass().equals(MainActivity.class)) {
            txtAviso.setText(getResources().getString(R.string.erro_dados));
            alertas.setNeutralButton(getResources().getString(R.string.fechar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Finalizar activity
                    finish();

                    // Apresentar animação de passagem da direita para esquerda
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                    // Fechar caixa de alerta
                    dialog.dismiss();
                }
            });

            alertas.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Recarregar activity
                    Intent intent = getIntent();
                    startActivity(intent);
                    finish();

                    // Não mostrar animação
                    overridePendingTransition(0, 0);
                }
            });
        } else {
            alertas.setNeutralButton(getResources().getString(R.string.fechar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Recarregar activity
                    Intent intent = getIntent();
                    startActivity(intent);
                    finish();

                    // Não mostrar animação
                    overridePendingTransition(0, 0);
                }
            });
        }

        alertas.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // Recarregar a caixa de alerta
                View dialoglayout = getLayoutInflater().inflate(R.layout.alert_dados, null);

                TextView txtTitulo = (TextView) dialoglayout.findViewById(R.id.txtTitulo);
                TextView txtAviso = (TextView) dialoglayout.findViewById(R.id.txtMensagem);

                adaptarTexto(txtTitulo);

                txtTitulo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Medium.ttf"));
                txtAviso.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

                // Adaptar texto da janela
                if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
                    txtTitulo.setTextSize(15);
                    txtAviso.setTextSize(15);

                    txtTitulo = aumentaTexto(txtTitulo);
                    txtAviso = aumentaTexto(txtAviso);

                    // Aumentar tamanho do ícone da janela
                    Drawable drawable = getResources().getDrawable(R.mipmap.ic_error_outline_white_24dp);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    int tamanho = ((int) txtTitulo.getTextSize());
                    drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));

                    txtTitulo.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                }

                alertas.setView(dialoglayout);
                alertas.show();
            }
        });

        alertas.show();
    }

    // Exibir mensagem de sessão expirada
    public void invalidarLogin(final Context context) {
        Log.e("INVALIDOU", "aqui");

        final AlertDialog.Builder alertas = new AlertDialog.Builder(context);

        View dialoglayout = getLayoutInflater().inflate(R.layout.alert_expired, null);

        TextView txtExpirada = (TextView) dialoglayout.findViewById(R.id.txtExpirada);
        TextView txtMensagem = (TextView) dialoglayout.findViewById(R.id.txtMensagem);

        // Adaptar texto da janela
        if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
            txtExpirada.setTextSize(15);
            txtMensagem.setTextSize(15);

            txtExpirada = aumentaTexto(txtExpirada);
            txtMensagem = aumentaTexto(txtMensagem);

            // Aumentar tamanho do ícone da janela
            Drawable drawable = getResources().getDrawable(R.mipmap.ic_error_outline_white_24dp);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            int tamanho = ((int) txtExpirada.getTextSize());
            drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));

            txtExpirada.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        }

        // Mudar cor da janela
        if (buscarTema().equals("azul")) {
            txtExpirada.setBackgroundColor(getResources().getColor(R.color.azulLogo));
        } else {
            txtExpirada.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
        }

        // Mudar fonte
        txtExpirada.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Medium.ttf"));
        txtMensagem.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

        alertas.setView(dialoglayout);

        alertas.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Mostrar a tela de Login
                finish();
                Intent resultado = new Intent(context, LoginActivity.class);
                resultado.putExtra("saiu", true);
                startActivity(resultado);

                // Apresentar animação de passagem de direita para esquerda
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        alertas.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                View dialoglayout = getLayoutInflater().inflate(R.layout.alert_expired, null);

                TextView txtExpirada = (TextView) dialoglayout.findViewById(R.id.txtExpirada);
                TextView txtMensagem = (TextView) dialoglayout.findViewById(R.id.txtMensagem);

                // Adaptar texto da janela
                if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
                    txtExpirada.setTextSize(15);
                    txtMensagem.setTextSize(15);

                    txtExpirada = aumentaTexto(txtExpirada);
                    txtMensagem = aumentaTexto(txtMensagem);

                    // Aumentar tamanho do ícone da janela
                    Drawable drawable = getResources().getDrawable(R.mipmap.ic_error_outline_white_24dp);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    int tamanho = ((int) txtExpirada.getTextSize());
                    drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));

                    txtExpirada.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                }

                // Mudar cor da janela
                if (buscarTema().equals("azul")) {
                    txtExpirada.setBackgroundColor(getResources().getColor(R.color.azulLogo));
                } else {
                    txtExpirada.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
                }

                // Mudar fonte
                txtExpirada.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Medium.ttf"));
                txtMensagem.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

                alertas.setView(dialoglayout);
                alertas.show();
            }
        });

        alertas.show();
    }
}
