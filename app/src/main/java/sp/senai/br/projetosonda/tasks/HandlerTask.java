package sp.senai.br.projetosonda.tasks;

/**
 * Created by José Roberto on 09/03/2017.
 */

public interface HandlerTask {
    void onPreHandle();

    void onSuccess(String valueRead);

    void onError(Exception erro);
}
