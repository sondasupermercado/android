package sp.senai.br.projetosonda.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.model.Aplicacao;

public class TreinamentoAdapter extends BaseAdapter {
    private final Context contexto;
    private final List<Aplicacao> aplicacoes;

    public TreinamentoAdapter(Context contexto, List<Aplicacao> aplicacoes) {
        this.contexto = contexto;
        this.aplicacoes = aplicacoes;
    }

    @Override
    public int getCount() {
        return aplicacoes.size();
    }

    @Override
    public Object getItem(int position) {
        return aplicacoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return aplicacoes.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(contexto).inflate(R.layout.adapter_treinamento, parent, false);

        Aplicacao aplicacao = aplicacoes.get(position);

        TextView txtTitulo = (TextView) view.findViewById(R.id.nameTreinamento);
        TextView txtStatus = (TextView) view.findViewById(R.id.statusTreinamento);

        txtTitulo.setText(view.getResources().getString(R.string.nome_treinamento, String.valueOf(aplicacao.getTreinamento().getConteudo().getNome().trim())));

        String status;

        // Pegar status do treinamento
        switch (aplicacao.getAprovado()) {
            case APROVADO:
                status = view.getResources().getString(R.string.aprovado);
                break;
            case REPROVADO:
                status = view.getResources().getString(R.string.reprovado);
                break;
            default:
                status = view.getResources().getString(R.string.pendente);
        }

        txtStatus.setText(Html.fromHtml(view.getResources().getString(R.string.status, status)));

        // Mudar fonte do texto
        txtTitulo.setTypeface(Typeface.createFromAsset(contexto.getResources().getAssets(), "fonts/HindVadodara-Medium.ttf"));
        txtStatus.setTypeface(Typeface.createFromAsset(contexto.getResources().getAssets(), "fonts/HindVadodara-Regular.ttf"));

        // Dimensões da Tela
        DisplayMetrics display = contexto.getResources().getDisplayMetrics();

        // Adaptar o tamanho do texto
        if (display.widthPixels <= 480 && display.heightPixels <= 800) {
            txtTitulo.setTextSize(17);
            txtStatus.setTextSize(17);
        } else if (display.widthPixels <= 800 && display.heightPixels <= 480) {
            txtTitulo.setTextSize(15);
            txtStatus.setTextSize(15);
        } else if (display.widthPixels <= 1794 && display.heightPixels <= 1080) {
            txtTitulo.setTextSize(20);
            txtStatus.setTextSize(20);
        } else if ((display.widthPixels >= 1600 && display.heightPixels >= 2464) || (display.heightPixels >= 1600 && display.widthPixels >= 2464)) {
            txtTitulo.setTextSize(15);
            txtStatus.setTextSize(15);

            txtTitulo.setTextSize(txtTitulo.getTextSize() + (txtTitulo.getTextSize() / 250));
            txtStatus.setTextSize(txtStatus.getTextSize() + (txtStatus.getTextSize() / 250));
        }

        return view;
    }
}
