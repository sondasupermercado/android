package sp.senai.br.projetosonda.model;

import java.util.Date;

/**
 * Created by Sonda on 03/04/2017.
 */

public class Funcionario {
    private Long id;
    private String nome;
    private String email;
    private boolean ativo = true;
    private String dataAdmissao;
    private Tipo tipo;
    private Cargo cargo;
    private String senha;
    private String apiToken;
    private Date apiTokenExpiracao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(String dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Date getApiTokenExpiracao() {
        return apiTokenExpiracao;
    }

    public void setApiTokenExpiracao(Date apiTokenExpiracao) {
        this.apiTokenExpiracao = apiTokenExpiracao;
    }
}
