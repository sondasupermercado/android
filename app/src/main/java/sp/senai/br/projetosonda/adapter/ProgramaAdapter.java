package sp.senai.br.projetosonda.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.model.Aprovado;
import sp.senai.br.projetosonda.model.Programa;

public class ProgramaAdapter extends BaseAdapter implements Filterable {
    private final Context contexto;
    private ItemFilter filter = new ItemFilter();
    public List<Programa> programas;
    public List<Programa> filtrados;

    public ProgramaAdapter(Context contexto, List<Programa> programas) {
        this.contexto = contexto;
        this.programas = programas;
        filtrados = programas;
    }

    @Override
    public int getCount() {
        return programas.size();
    }

    @Override
    public Object getItem(int position) {
        return programas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return programas.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(contexto).inflate(R.layout.adapter_programa, parent, false);

        Programa programa = programas.get(position);

        TextView txtTitulo = (TextView) view.findViewById(R.id.nameTitulo);
        Drawable drawable = view.getResources().getDrawable(R.mipmap.ic_assignment_black_24dp);

        // Mudar ícone
        if (programa.getAprovado() == Aprovado.APROVADO) {
            drawable = view.getResources().getDrawable(R.mipmap.ic_assignment_green_24dp);
        } else if (programa.getAprovado() == Aprovado.REPROVADO) {
            drawable = view.getResources().getDrawable(R.mipmap.ic_assignment_red_24dp);
        }

        // Adicionar texto e modificar fonte
        txtTitulo.setText(programa.getNome());
        txtTitulo.setTypeface(Typeface.createFromAsset(view.getResources().getAssets(), "fonts/HindVadodara-Regular.ttf"));

        DisplayMetrics display = contexto.getResources().getDisplayMetrics();

        // Adaptar o tamanho do texto
        if (display.widthPixels <= 480 && display.heightPixels <= 800) {
            txtTitulo.setPadding(10, 5, 5, 5);
        } else if ((display.widthPixels >= 1600 && display.heightPixels >= 2464) || (display.heightPixels >= 1600 && display.widthPixels >= 2464)) {
            txtTitulo.setTextSize(20);
            txtTitulo.setTextSize(txtTitulo.getTextSize() + (txtTitulo.getTextSize() / 250));

            // Aumentar o tamanho do ícone
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            int tamanho = ((int) txtTitulo.getTextSize());
            drawable = new BitmapDrawable(view.getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));
        }

        txtTitulo.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        return view;
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new ItemFilter();
        }

        return filter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            // Fazer filtragem dos programas pela actionbar

            if (constraint != null && constraint.length() > 0) {
                ArrayList<Programa> listaFiltrada = new ArrayList<>();
                for (int i = 0; i < filtrados.size(); i++) {

                    // Comparar nome do programa com o texto inserido e com o ID
                    if (filtrados.get(i).getNome().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        Programa programaAtual = new Programa();
                        programaAtual.setNome(filtrados.get(i).getNome());
                        programaAtual.setId(filtrados.get(i).getId());
                        programaAtual.setAprovado(filtrados.get(i).getAprovado());
                        listaFiltrada.add(programaAtual);
                    }
                }

                results.count = listaFiltrada.size();
                results.values = listaFiltrada;

            } else {
                results.count = filtrados.size();
                results.values = filtrados;
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            programas = (ArrayList<Programa>) results.values;
            notifyDataSetChanged();
        }

    }
}
