package sp.senai.br.projetosonda.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.model.RedefinirSenha;
import sp.senai.br.projetosonda.tasks.HandlerTask;
import sp.senai.br.projetosonda.tasks.HandlerTaskAdapter;
import sp.senai.br.projetosonda.tasks.TaskRest;
import sp.senai.br.projetosonda.util.JsonParser;

import static sp.senai.br.projetosonda.view.SuperClassActivity.IP;

/**
 * Created by Sonda on 17/04/2017.
 */

public class NewPasswordActivity extends AppCompatActivity {
    private String urlRecuperar = "http://" + IP + "/GestaoDeTreinamento/funcionario/redefinirSenha";

    private TextView txtTitulo, txtDescricao, txtCodigo, txtNovaSenha, txtConfirma;
    private View linha_1, linha_2;
    protected Toolbar toolbar;
    private Button btnLogin;

    protected int larguraTela, alturaTela;
    private int contador;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        toolbar.setTitle(R.string.redefinir);
        setSupportActionBar(toolbar);

        // Adicionar botão voltar na ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Obter dimensões da tela
        atribuirDimensoes();

        // Inicializar tentativas
        contador = 0;

        // Mudar fonte do título

        txtConfirma = (TextView) findViewById(R.id.txtConfirmaSenha);
        txtDescricao = (TextView) findViewById(R.id.txtDescricao);
        txtNovaSenha = (TextView) findViewById(R.id.txtNovaSenha);
        txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        txtCodigo = (TextView) findViewById(R.id.txtCodigo);
        btnLogin = (Button) findViewById(R.id.btn_login);
        linha_1 = findViewById(R.id.view_linha_1);
        linha_2 = findViewById(R.id.view_linha_2);

        // Mudar fonte
        txtTitulo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));
        txtConfirma.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));
        txtDescricao.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));
        txtNovaSenha.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));
        txtCodigo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));
        btnLogin.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));

        // Adaptar conteúdo à tela
        adaptarConteudo();
    }

    // Métodos de requisição

    // Mudar a senha do funcionário
    public void mudarSenha(View view) {
        Pattern pattern = Pattern.compile("(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&()_\\-])[A-Za-z\\d$@$!%*#?&()_\\-]{8,}");
        Matcher matcher = pattern.matcher(txtNovaSenha.getEditableText().toString());

        if (txtCodigo.getEditableText().toString().trim().isEmpty() || txtNovaSenha.getEditableText().toString().trim().isEmpty() || txtConfirma.getEditableText().toString().trim().isEmpty()) {
            Toast.makeText(NewPasswordActivity.this, getResources().getString(R.string.vazio), Toast.LENGTH_SHORT).show();
        } else {
            if (!txtNovaSenha.getEditableText().toString().equals(txtConfirma.getEditableText().toString())) {
                Toast.makeText(NewPasswordActivity.this, getResources().getString(R.string.combinam), Toast.LENGTH_SHORT).show();
            } else if (txtNovaSenha.getEditableText().length() < 8) {
                Toast.makeText(NewPasswordActivity.this, getResources().getString(R.string.erro_tamanho), Toast.LENGTH_SHORT).show();
            } else if (txtNovaSenha.getEditableText().toString().equalsIgnoreCase("null") || !matcher.matches()) {
                contador++;

                if (contador <= 2) {
                    final AlertDialog.Builder alertas = new AlertDialog.Builder(NewPasswordActivity.this);
                    View dialoglayout = getLayoutInflater().inflate(R.layout.alert_senha, null);

                    TextView txtTitulo = (TextView) dialoglayout.findViewById(R.id.txtTitulo);
                    TextView txtAviso = (TextView) dialoglayout.findViewById(R.id.txtMensagem);

                    txtTitulo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Medium.ttf"));
                    txtAviso.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

                    alertas.setView(dialoglayout);

                    alertas.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertas.show();
                } else {
                    Toast.makeText(NewPasswordActivity.this, getResources().getString(R.string.senha_invalida), Toast.LENGTH_SHORT).show();
                }
            } else {
                RedefinirSenha redefinirSenha = new RedefinirSenha();
                redefinirSenha.setSenha(txtNovaSenha.getEditableText().toString());
                redefinirSenha.setCodigo(txtCodigo.getEditableText().toString());
                JsonParser<RedefinirSenha> parser = new JsonParser<>(RedefinirSenha.class);
                new TaskRest(TaskRest.RequestMethod.POST, handlerTask, NewPasswordActivity.this).execute(urlRecuperar, parser.fromObject(redefinirSenha));
            }
        }
    }

    // Validar requisição
    private HandlerTask handlerTask = new HandlerTaskAdapter() {
        @Override
        public void onError(Exception erro) {
            Log.e("ERRO", erro.getMessage());

            if (erro.getMessage().contains("403")) {
                Toast.makeText(getBaseContext(), getResources().getString(R.string.erro_codigo), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getBaseContext(), getResources().getString(R.string.erro_conexao), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccess(String retorno) {
            Toast.makeText(NewPasswordActivity.this, getResources().getString(R.string.sucesso), Toast.LENGTH_SHORT).show();

            // Voltar à tela de login
            startActivity(new Intent(getBaseContext(), LoginActivity.class));
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            finish();
        }
    };

    // Métodos da classe

    // Obter as dimensões da tela
    public void atribuirDimensoes() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        larguraTela = size.x;
        alturaTela = size.y;
    }

    // Adaptar conteúdo à tela
    private void adaptarConteudo() {
        if ((larguraTela <= 480 && alturaTela <= 800) || (larguraTela <= 800 && alturaTela <= 480)) {
            txtTitulo.setTextSize(35);

            txtDescricao.setTextSize(20);
            txtNovaSenha.setTextSize(20);
            txtConfirma.setTextSize(20);
            txtCodigo.setTextSize(20);
            btnLogin.setTextSize(17);
        } else if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
            // Adaptar tamanho do texto
            txtTitulo.setTextSize(35);
            txtTitulo.setTextSize(txtTitulo.getTextSize() + (txtTitulo.getTextSize() / 250));
            txtTitulo.setPadding(0, 0, 0, 65);

            txtDescricao.setTextSize(17);
            txtDescricao.setPadding(0, 35, 0, 35);
            txtDescricao.setTextSize(txtDescricao.getTextSize() + (txtDescricao.getTextSize() / 250));

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(85, 45, 85, 35);

            txtCodigo.setLayoutParams(params);
            txtCodigo.setTextSize(17);
            txtCodigo.setPadding(0, 35, 10, 35);
            txtCodigo.setTextSize(txtCodigo.getTextSize() + (txtCodigo.getTextSize() / 250));

            params.setMargins(85, 0, 85, 35);

            txtNovaSenha.setLayoutParams(params);
            txtNovaSenha.setTextSize(17);
            txtNovaSenha.setPadding(0, 35, 10, 35);
            txtNovaSenha.setTextSize(txtNovaSenha.getTextSize() + (txtNovaSenha.getTextSize() / 250));

            txtConfirma.setLayoutParams(params);
            txtConfirma.setTextSize(17);
            txtConfirma.setPadding(0, 35, 10, 35);
            txtConfirma.setTextSize(txtConfirma.getTextSize() + (txtConfirma.getTextSize() / 250));

            // Redimencionar a linha do título
            LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParam.height = 5;
            layoutParam.setMargins(85, 45, 85, 45);
            linha_1.setLayoutParams(layoutParam);
            linha_2.setLayoutParams(layoutParam);

            // Alterar tamanho do botão
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 75, 0, 0);
            //btnLogin.setLayoutParams(layoutParams);

            btnLogin.setTextSize(17);
            btnLogin.setTextSize(btnLogin.getTextSize() + (btnLogin.getTextSize() / 250));
        }
    }

    // Finalizar activity quando clicar em voltar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                // Apresentar animação de passagem de direita para esquerda
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Finalizar a activity quando clicar em voltar do celular
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Finalizar activity
        finish();

        // Apresentar animação de passagem de direita para esquerda
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
