package sp.senai.br.projetosonda.view;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Date;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.model.Funcionario;
import sp.senai.br.projetosonda.tasks.HandlerTask;
import sp.senai.br.projetosonda.tasks.HandlerTaskAdapter;
import sp.senai.br.projetosonda.tasks.TaskRest;
import sp.senai.br.projetosonda.util.JsonParser;
import sp.senai.br.projetosonda.util.Permissoes;

import static sp.senai.br.projetosonda.view.MainActivity.THEME;
import static sp.senai.br.projetosonda.view.SuperClassActivity.APP_PREFS;
import static sp.senai.br.projetosonda.view.SuperClassActivity.EMAIL;
import static sp.senai.br.projetosonda.view.SuperClassActivity.ID;
import static sp.senai.br.projetosonda.view.SuperClassActivity.IP;
import static sp.senai.br.projetosonda.view.SuperClassActivity.LOGADO;
import static sp.senai.br.projetosonda.view.SuperClassActivity.SENHA;
import static sp.senai.br.projetosonda.view.SuperClassActivity.TOKEN;

public class LoginActivity extends AppCompatActivity {
    private final String url = "http://" + IP + "/GestaoDeTreinamento/funcionario/login";

    protected int larguraTela, alturaTela;
    protected String erros, email, senha;

    private TextView editEmail, editSenha, txtEsqueciSenha;
    private CheckBox check_logado;
    private ImageView imgLogo;
    private Button btnLogin;

    public Funcionario funcionario;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_login);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            // Validar o logout
            if (bundle.getBoolean("saiu")) {
                manterLogado(false);
            }
            // Renovar o login
            if (bundle.getBoolean("renovar")) {
                Log.e("NOVO LOGIN", "Renovou o login");
                renovarLogin();
            }
        }

        // Permissões
        String[] permissoes = new String[2];

        permissoes[0] = Manifest.permission.INTERNET;
        permissoes[1] = Manifest.permission.ACCESS_NETWORK_STATE;

        Permissoes.checarPermissoes(this, 1, permissoes);

        // Obter as dimensões da tela
        atribuirDimensoes();

        // Verificar se o funcionário está logado
        if (verificarLogin()) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            check_logado = (CheckBox) findViewById(R.id.check_logado);
            txtEsqueciSenha = (TextView) findViewById(R.id.txtLogin);
            editEmail = (TextView) findViewById(R.id.edit_email);
            editSenha = (TextView) findViewById(R.id.edit_senha);
            imgLogo = (ImageView) findViewById(R.id.img_logo);
            btnLogin = (Button) findViewById(R.id.btn_login);

            // Mudar fonte
            check_logado.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
            txtEsqueciSenha.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
            editEmail.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
            editSenha.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
            btnLogin.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

            // Adaptar conteúdo à tela
            adaptarConteudo();
        }
    }

    // Métodos de requisição

    // Verificar permissões
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int resultado : grantResults) {
            if (resultado == PackageManager.PERMISSION_DENIED) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                builder.setMessage(getResources().getString(R.string.permissao));
                AlertDialog dialog = builder.create();
                dialog.show();
            }

        }
    }

    // Métodos de requisição

    // Realizar a requisição de login
    public void realizarLogin(View view) {
        if (editEmail.getEditableText().toString().trim().isEmpty() || editSenha.getEditableText().toString().trim().isEmpty()) {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.vazio), Toast.LENGTH_SHORT).show();

        } else {
            Funcionario f = new Funcionario();
            f.setEmail(editEmail.getEditableText().toString());
            f.setSenha(editSenha.getEditableText().toString());
            JsonParser<Funcionario> parser = new JsonParser<>(Funcionario.class);
            new TaskRest(TaskRest.RequestMethod.POST, handlerTask, LoginActivity.this).execute(url, parser.fromObject(f));
        }
    }

    // Validar Login
    public HandlerTask handlerTask = new HandlerTaskAdapter() {
        @Override
        public void onError(Exception erro) {
            Log.e("ERRO", erro.getMessage());
            if (erro.getMessage().contains("401")) {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                email = String.valueOf(prefs.getString(EMAIL, null));
                senha = String.valueOf(prefs.getString(SENHA, null));

                Log.e("EMAIL", email);
                Log.e("SENHA", senha);

                if (!email.equals("null") && !senha.equals("null")) {
                    // Voltar ao tema padrão
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(THEME, "azul");
                    editor.putString(EMAIL, null);
                    editor.putString(SENHA, null);
                    editor.commit();

                    final AlertDialog.Builder alertas = new AlertDialog.Builder(LoginActivity.this);

                    View dialoglayout = getLayoutInflater().inflate(R.layout.alert_expired, null);

                    TextView txtExpirada = (TextView) dialoglayout.findViewById(R.id.txtExpirada);

                    if (buscarTema().equals("azul")) {
                        txtExpirada.setBackgroundColor(getResources().getColor(R.color.azulLogo));
                    } else {
                        txtExpirada.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
                    }

                    alertas.setView(dialoglayout);

                    alertas.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Mostrar a tela de Login
                            manterLogado(false);

                            finish();
                            Intent resultado = new Intent(getIntent());
                            startActivity(resultado);

                            overridePendingTransition(0, 0);
                        }
                    });

                    alertas.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            View dialoglayout = getLayoutInflater().inflate(R.layout.alert_expired, null);

                            TextView txtExpirada = (TextView) dialoglayout.findViewById(R.id.txtExpirada);

                            if (buscarTema().equals("azul")) {
                                txtExpirada.setBackgroundColor(getResources().getColor(R.color.azulLogo));
                            } else {
                                txtExpirada.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
                            }

                            alertas.setView(dialoglayout);
                            alertas.show();
                        }
                    });

                    alertas.show();
                } else {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.erro_login), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getBaseContext(), getResources().getString(R.string.erro_conexao), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccess(String retorno) {
            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (!retorno.equals("")) {
                    JSONObject fJson = new JSONObject(retorno);
                    funcionario = form.fromJson(fJson.toString(), Funcionario.class);

                    Log.e("FUN", retorno);
                } else {
                    erros = "Funcionário não encontrado";
                }

            } catch (JSONException e) {
                e.printStackTrace();
                erros = e.getMessage();
            }

            if (erros == null) {
                // Armazenar o token do funcionário para a realização de requisições
                salvarToken(funcionario.getApiToken());

                // Salvar email e senha
                if (editEmail != null && editSenha != null) {
                    salvarLogin(editEmail.getEditableText().toString(), editSenha.getEditableText().toString());

                    SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                    email = String.valueOf(prefs.getString(EMAIL, null));
                    senha = String.valueOf(prefs.getString(SENHA, null));

                    Log.e("EMAIL E SENHA", email + " - " + senha);
                }

                // Verificar se o usuário manteve-se logado
                if (check_logado != null) {
                    manterLogado(check_logado.isChecked());
                } else {
                    manterLogado(true);
                }

                // Salvar o ID do funcionário
                salvarIdFuncionario(funcionario.getId());

                // Iniciar Tela Inicial
                startActivity(new Intent(getBaseContext(), MainActivity.class));

                overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                finish();
            } else {
                Toast.makeText(getBaseContext(), getResources().getString(R.string.erro_conexao), Toast.LENGTH_SHORT).show();
            }
        }
    };

    // Métodos da classe

    // Buscar a cor do tema
    public String buscarTema() {
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        String cor = prefs.getString(THEME, "azul");
        return cor;
    }

    // Recuperar a senha do funcionário
    public void recuperarSenha(View view) {
        startActivity(new Intent(getBaseContext(), RecoverActivity.class));
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    // Armazenar o token do funcionário para a realização de requisições
    private void salvarToken(String token) {
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(TOKEN, token);
        editor.commit();
    }

    // Salvar e-mail e senha
    private void salvarLogin(String email, String senha) {
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(EMAIL, email);
        editor.putString(SENHA, senha);
        editor.commit();
    }

    // Verificar se o usuário manteve-se logado
    public void manterLogado(boolean isLogado) {
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(LOGADO, isLogado);
        editor.commit();
    }

    // Verificar se o funcionário está logado
    private boolean verificarLogin() {
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        boolean status = prefs.getBoolean(LOGADO, false);

        return status;
    }

    // Armazenar o código do funcionário
    private void salvarIdFuncionario(Long id) {
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(ID, id);
        editor.commit();
    }

    // Obter as dimensões da tela
    public void atribuirDimensoes() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        larguraTela = size.x;
        alturaTela = size.y;
    }

    // Renovar o login quando a sessão expirar
    private void renovarLogin() {
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        email = String.valueOf(prefs.getString(EMAIL, null));
        senha = String.valueOf(prefs.getString(SENHA, null));

        Log.e("EMAIL E SENHA", email + " - " + senha);

        if (!email.equals("null") && !senha.equals("null")) {
            // Refazer o login automaticamente
            Funcionario f = new Funcionario();
            f.setEmail(email);
            f.setSenha(senha);
            JsonParser<Funcionario> parser = new JsonParser<>(Funcionario.class);
            new TaskRest(TaskRest.RequestMethod.POST, handlerTask, LoginActivity.this).execute(url, parser.fromObject(f));
            Log.e("FOI", "login automático");
        } else {
            // Problema no Login
            Log.e("NÂO FOI", "login manual");

            manterLogado(false);
            Intent resultado = new Intent(this, MainActivity.class);
            resultado.putExtra("expirado", true);
            startActivity(resultado);
        }

        Log.e("LOGADO", String.valueOf(verificarLogin()));
    }

    // Método da classe

    // Adaptar conteúdo à tela
    private void adaptarConteudo() {
        if ((larguraTela <= 480 && alturaTela <= 800) || (larguraTela <= 800 && alturaTela <= 480)) {
            editEmail.setTextSize(20);
            editSenha.setTextSize(20);

            txtEsqueciSenha.setTextSize(17);
            check_logado.setTextSize(17);
            btnLogin.setTextSize(17);
        } else if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 45, 0, 45);

            check_logado.setLayoutParams(layoutParams);
            check_logado.setTextSize(15);
            check_logado.setTextSize(check_logado.getTextSize() + (check_logado.getTextSize() / 250));

            // Alterar tamanho do ícone
            imgLogo.setImageDrawable(getResources().getDrawable(R.drawable.logo_sonda_200));
            imgLogo.setPadding(0, 0, 0, 65);

            txtEsqueciSenha.setTextSize(15);
            txtEsqueciSenha.setTextSize(txtEsqueciSenha.getTextSize() + (txtEsqueciSenha.getTextSize() / 250));

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(85, 0, 85, 35);
            editEmail.setLayoutParams(params);
            editEmail.setTextSize(17);
            editEmail.setTextSize(editEmail.getTextSize() + (editEmail.getTextSize() / 250));

            editSenha.setLayoutParams(params);
            editSenha.setTextSize(17);
            editSenha.setTextSize(editSenha.getTextSize() + (editSenha.getTextSize() / 250));

            layoutParams.setMargins(0, 35, 0, 35);
            btnLogin.setLayoutParams(layoutParams);

            // Alterar tamanho do botão
            btnLogin.setTextSize(15);
            btnLogin.setTextSize(btnLogin.getTextSize() + (btnLogin.getTextSize() / 250));
            btnLogin.setPadding(55, 25, 55, 25);
        }
    }

    // Quando voltar à tela de login
    @Override
    protected void onResume() {
        super.onResume();

        // Verificar se o funcionário está logado
        if (verificarLogin()) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            check_logado = (CheckBox) findViewById(R.id.check_logado);
            editEmail = (TextView) findViewById(R.id.edit_email);
            editSenha = (TextView) findViewById(R.id.edit_senha);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Minimizar a tela
        moveTaskToBack(true);
    }
}
