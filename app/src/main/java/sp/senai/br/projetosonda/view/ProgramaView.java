package sp.senai.br.projetosonda.view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.adapter.TreinamentoAdapter;
import sp.senai.br.projetosonda.model.Aplicacao;
import sp.senai.br.projetosonda.model.Programa;
import sp.senai.br.projetosonda.util.RestUtil;

/**
 * Created by Sonda on 03/04/2017.
 */

public class ProgramaView extends SuperClassActivity {
    private TextView txtNome, txtDuracao, txtReciclagem, txtTreinamentos;
    private ProgressDialog progresso;
    private ListView listView;
    private View viewLinha;

    private Programa programaAtual;

    private Long id_programa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String cor = buscarTema();
        if (cor.equals("azul")) {
            setTheme(R.style.AppTheme);
        } else {
            setTheme(R.style.AlternativeAppTheme);
        }

        setContentView(R.layout.programa_view);

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        toolbar.setTitle(R.string.descricao);

        // Alterar cor do tema
        atualizarTema(cor, toolbar);

        // Código do funcionário
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        id_funcionario = prefs.getLong(ID, 1);

        // Pegar programa selecionado
        id_programa = getIntent().getExtras().getLong("id_programa");
        setSupportActionBar(toolbar);

        // Adicionar botão voltar na ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtReciclagem = (TextView) findViewById(R.id.reciclagem_programa);
        txtTreinamentos = (TextView) findViewById(R.id.txtTreinamentos);
        txtDuracao = (TextView) findViewById(R.id.duracao_programa);
        listView = (ListView) findViewById(R.id.list_treinamentos);
        txtNome = (TextView) findViewById(R.id.nome_programa);
        viewLinha = findViewById(R.id.view_linha);

        // Mudar fonte
        txtTreinamentos.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
        txtReciclagem.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
        txtDuracao.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
        txtNome.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf"));

        // Obter dimensões da tela
        atribuirDimensoes();

        // Instancia a barra de progresso
        progresso = new ProgressDialog(ProgramaView.this);
        progresso.setTitle(getResources().getString(R.string.aguarde));
        progresso.setMessage(getResources().getString(R.string.consultando));

        // Visualizar as informações do Programa
        new LerPrograma().execute();

        adaptarConteudo();

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);

        // Atualizar lista quando o primeiro primeiro elemento visível é o primeiro elemento da lista
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // Verificar se o primeiro elemento visível é o primeiro elemento da listview
                int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();

                // Permitir que a lista atualize
                swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });

        // Realizar ação ao recarregar a tela
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Visualizar as informações do Programa
                new LerPrograma().execute();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    // Métodos de requisição

    // Visualizar as informações do Programa
    public class LerPrograma extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progresso.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String retorno = null;

            try {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                String token = String.valueOf(prefs.getString(TOKEN, null));

                retorno = RestUtil.get(urlPrograma + id_programa, ProgramaView.this, token);

            } catch (IOException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (retorno != null) {
                    if (!retorno.equals("")) {
                        JSONObject pJson = new JSONObject(retorno);
                        programaAtual = form.fromJson(pJson.toString(), Programa.class);

                    } else {
                        if (!contemErros) {
                            contemErros = true;
                            erros = "Dados não encontrados";
                        }
                    }
                } else {
                    if (!contemErros) {
                        contemErros = true;
                        erros = "Dados não encontrados";
                    }
                }

            } catch (JSONException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (erros == null) {
                txtNome.setText(programaAtual.getNome());


                // Se o nome do programa é extenso
                if ((txtNome.getText().length() >= 25) && (larguraTela < 1600 && alturaTela < 2464)) {
                    if (larguraTela <= 480 && alturaTela <= 800) {
                        txtNome.setTextSize(25);
                    } else {
                        txtNome.setTextSize(33);
                    }
                }

                if (programaAtual.getDuracao() > 1) {
                    txtDuracao.setText(getString(R.string.duracao, String.valueOf(programaAtual.getDuracao())));
                } else {
                    txtDuracao.setText(getString(R.string.duracao_singular));
                }

                // Se o programa tem reciclagem
                if (programaAtual.getPrazoReciclagem() >= 1) {
                    if (programaAtual.getPrazoReciclagem() != 1) {
                        txtReciclagem.setText(getString(R.string.reciclagem, String.valueOf(programaAtual.getPrazoReciclagem())));
                    } else {
                        txtReciclagem.setText(getString(R.string.reciclagem_singular));
                    }

                } else {
                    txtReciclagem.setVisibility(View.GONE);
                }

                txtTreinamentos.setVisibility(View.VISIBLE);

                // Adaptar texto
                if (larguraTela <= 480 && alturaTela <= 800) {
                    txtTreinamentos.setTextSize(22);
                    txtReciclagem.setTextSize(18);
                    txtDuracao.setTextSize(18);
                    txtNome.setTextSize(35);
                }

                viewLinha.setVisibility(View.VISIBLE);

                new BuscarTreinamentos().execute();
            } else {
                tratarErro(ProgramaView.this);
            }
        }
    }

    // Buscar os treinamentos do programa atual
    public class BuscarTreinamentos extends AsyncTask<Void, Void, String> {
        private List<Aplicacao> treinamentos = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String retorno = null;

            try {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                String token = String.valueOf(prefs.getString(TOKEN, null));

                retorno = RestUtil.get(urlAplicacoes + id_funcionario + "/funcionario", ProgramaView.this, token);
            } catch (IOException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (retorno != null) {
                    JSONArray jsonArray = new JSONArray(retorno);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject aplicacaoJson = jsonArray.getJSONObject(i);
                        Aplicacao aplicacao = form.fromJson(aplicacaoJson.toString(), Aplicacao.class);

                        // Adicionar treinamentos do programa
                        if (aplicacao.getTreinamento() != null && aplicacao.getTreinamento().getConteudo() != null) {
                            if (aplicacao.getTreinamento().getPrograma().getId() == programaAtual.getId()) {
                                treinamentos.add(aplicacao);
                            }
                        }
                    }
                } else {
                    contemErros = true;
                    erros = "Dados não encontrados";
                }

            } catch (JSONException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
            progresso.dismiss();

            if (erros == null) {
                listView.setAdapter(new TreinamentoAdapter(ProgramaView.this, treinamentos));

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Aplicacao aplicacao = (Aplicacao) parent.getItemAtPosition(position);

                        if (aplicacao.getTreinamento().getConteudo().getObjetivo() != null) {
                            String objetivo = aplicacao.getTreinamento().getConteudo().getObjetivo().substring(0, 1).toUpperCase() +
                                    aplicacao.getTreinamento().getConteudo().getObjetivo().substring(1).toLowerCase();

                            AlertDialog.Builder alerta = new AlertDialog.Builder(ProgramaView.this);
                            View dialoglayout = getLayoutInflater().inflate(R.layout.alert_treinamento, null);

                            TextView txtTitulo = (TextView) dialoglayout.findViewById(R.id.txtTitulo);
                            TextView txtObjetivo = (TextView) dialoglayout.findViewById(R.id.txtObjetivo);
                            TextView txtHorario = (TextView) dialoglayout.findViewById(R.id.txtCargaHoraria);

                            if (buscarTema().equals("azul")) {
                                txtTitulo.setBackgroundColor(getResources().getColor(R.color.azulLogo));
                            } else {
                                txtTitulo.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
                            }

                            txtTitulo.setText(aplicacao.getTreinamento().getConteudo().getNome().trim());
                            txtObjetivo.setText(Html.fromHtml(getResources().getString(R.string.objetivo, objetivo.trim())));

                            adaptarTexto(txtTitulo);

                            alerta.setView(dialoglayout);

                            if (aplicacao.getTreinamento().getConteudo().getCargaHoraria() != 1 && aplicacao.getTreinamento().getConteudo().getCargaHoraria() != 0) {
                                // Quando a carga horária é diferente de 1
                                txtHorario.setText(Html.fromHtml(view.getResources().getString(R.string.carga_horaria,
                                        String.valueOf(aplicacao.getTreinamento().getConteudo().getCargaHoraria()))));
                            } else if (aplicacao.getTreinamento().getConteudo().getCargaHoraria() != 0) {
                                // Quando a carga horária é igual a 1 e diferente de 0
                                txtHorario.setText(Html.fromHtml(view.getResources().getString(R.string.carga_horaria_singular)));
                            } else {
                                txtHorario.setVisibility(View.GONE);
                            }

                            // Adaptar texto da janela
                            if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
                                txtTitulo.setTextSize(20);
                                txtObjetivo.setTextSize(15);
                                txtHorario.setTextSize(15);

                                txtTitulo = aumentaTexto(txtTitulo);
                                txtObjetivo = aumentaTexto(txtObjetivo);
                                txtHorario = aumentaTexto(txtHorario);

                                // Aumentar tamanho do ícone da janela
                                Drawable drawable = getResources().getDrawable(R.mipmap.ic_description_white_24dp);
                                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                                int tamanho = ((int) txtTitulo.getTextSize());
                                drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));

                                txtTitulo.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                            }


                            alerta.setPositiveButton(getResources().getString(R.string.fechar), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            alerta.show();
                        }
                    }
                });
            } else {
                tratarErro(ProgramaView.this);
            }
        }
    }

    // Métodos da classe

    // Adaptar o conteúdo
    private void adaptarConteudo() {
        //int orientation = getResources().getConfiguration().orientation;

        // Diminuir fonte quando a tela estiver em modo paisagem e a tela for pequena
        if (larguraTela <= 800 && alturaTela <= 480) {
            txtTreinamentos.setTextSize(20);
            txtReciclagem.setTextSize(16.5f);
            txtDuracao.setTextSize(16.5f);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            layoutParams.setMargins(15, 5, 15, 15); // esquerda, cima, direita, baixo
            listView.setLayoutParams(layoutParams);

        } else if (larguraTela <= 1794 && alturaTela <= 1080) {
            // Diminuir fonte quando a tela estiver em modo paisagem
            txtTreinamentos.setTextSize(25);
            txtReciclagem.setTextSize(20);
            txtDuracao.setTextSize(20);
        } else if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            txtNome.setTextSize(35);
            txtTreinamentos.setTextSize(25);
            txtReciclagem.setTextSize(20);
            txtDuracao.setTextSize(20);

            layoutParams.height = 5;
            layoutParams.setMargins(10, 15, 10, 15);

            viewLinha.setLayoutParams(layoutParams);

            txtNome = aumentaTexto(txtNome);
            txtTreinamentos = aumentaTexto(txtTreinamentos);
            txtReciclagem = aumentaTexto(txtReciclagem);
            txtDuracao = aumentaTexto(txtDuracao);
        }
    }

    // Finalizar activity quando clicar em voltar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                // Apresentar animação de passagem de direita para esquerda
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Finalizar a activity quando clicar em voltar do celular
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Finalizar activity
        finish();

        // Apresentar animação de passagem de direita para esquerda
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    // Quando voltar à tela de programa
    @Override
    protected void onResume() {
        super.onResume();

        // Visualizar as informações do Programa
        new LerPrograma().execute();
    }
}
