package sp.senai.br.projetosonda.model;

import java.util.Date;

/**
 * Created by Sonda on 03/04/2017.
 */

public class Avaliacao {
    private Long id;
    private Programa programa;
    private Funcionario funcionarioAvaliado;
    private Funcionario responsavelAvaliacao;
    private Date dataAtual = new Date();
    private Date dataConclusao = null;
    private String status;
    private int somaNotas;
    private String baseAvaliacao;
    private String comentarios;
    private String sugestoes;
    private boolean ativo = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Funcionario getFuncionarioAvaliado() {
        return funcionarioAvaliado;
    }

    public void setFuncionarioAvaliado(Funcionario funcionarioAvaliado) {
        this.funcionarioAvaliado = funcionarioAvaliado;
    }

    public Funcionario getResponsavelAvaliacao() {
        return responsavelAvaliacao;
    }

    public void setResponsavelAvaliacao(Funcionario responsavelAvaliacao) {
        this.responsavelAvaliacao = responsavelAvaliacao;
    }

    public Date getDataAtual() {
        return dataAtual;
    }

    public void setDataAtual(Date dataAtual) {
        this.dataAtual = dataAtual;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSomaNotas() {
        return somaNotas;
    }

    public void setSomaNotas(int somaNotas) {
        this.somaNotas = somaNotas;
    }

    public String getBaseAvaliacao() {
        return baseAvaliacao;
    }

    public void setBaseAvaliacao(String baseAvaliacao) {
        this.baseAvaliacao = baseAvaliacao;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getSugestoes() {
        return sugestoes;
    }

    public void setSugestoes(String sugestoes) {
        this.sugestoes = sugestoes;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
