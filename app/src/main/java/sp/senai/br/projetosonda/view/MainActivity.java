package sp.senai.br.projetosonda.view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import sp.senai.br.projetosonda.R;
import sp.senai.br.projetosonda.model.Aplicacao;
import sp.senai.br.projetosonda.model.Aprovado;
import sp.senai.br.projetosonda.model.Funcionario;
import sp.senai.br.projetosonda.model.Operacao;
import sp.senai.br.projetosonda.model.Programa;
import sp.senai.br.projetosonda.util.RestUtil;

public class MainActivity extends SuperClassActivity {
    private final String urlFuncionario = "http://" + IP + "/GestaoDeTreinamento/funcionario/";
    private String urlOperacao = "http://" + IP + "/GestaoDeTreinamento/operacao/funcionario/";

    public ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private View linha_1, linha_2;
    private ImageView imageView;

    private TextView txtNome, txtCargo, txtOperacao, txtData, txtTitulo, txtEmail;
    private Funcionario funcionario;
    private Operacao operacao;
    private ProgressDialog progresso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Buscar a cor do tema
        String cor = buscarTema();
        if (cor.equals("azul")) {
            setTheme(R.style.AppTheme);
        } else {
            setTheme(R.style.AlternativeAppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Código do funcionário
        SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        id_funcionario = prefs.getLong(ID, 1);

        Log.e("ID", String.valueOf(id_funcionario));

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        toolbar.setTitle(R.string.titulo);

        // Atualizar a cor do tema
        atualizarTema(cor, toolbar);

        setSupportActionBar(toolbar);

        // Criar o menu lateral
        createActionBar();

        // Limpando tentativas de reconexão
        if (tentativas > 3) {
            tentativas = 0;
        }

        txtTitulo = (TextView) findViewById(R.id.txt_boas_vindas);
        txtOperacao = (TextView) findViewById(R.id.txtOperacao);
        imageView = (ImageView) findViewById(R.id.main_icon);
        txtCargo = (TextView) findViewById(R.id.txtCargo);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtNome = (TextView) findViewById(R.id.txtNome);
        txtData = (TextView) findViewById(R.id.txtData);
        linha_1 = findViewById(R.id.view_linha_1);
        linha_2 = findViewById(R.id.view_linha_2);

        // Mudar fonte
        txtOperacao.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Light.ttf"));
        txtTitulo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Regular.ttf"));
        txtCargo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Light.ttf"));
        txtEmail.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Light.ttf"));
        txtNome.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Light.ttf"));
        txtData.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Light.ttf"));

        // Obter dimensões da tela
        atribuirDimensoes();

        // Adaptar conteúdo à tela
        adaptarConteudo();

        Log.e("TAMANHO", alturaTela + " x " + larguraTela);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            // Invalidar sessão
            if (bundle.getBoolean("expirado")) {
                contemErros = true;
                erros = "Sessão Expirada";
                tratarErro(this);
            }
        } else {
            // Instancia a barra de progresso
            progresso = new ProgressDialog(MainActivity.this);
            progresso.setTitle(getResources().getString(R.string.aguarde));
            progresso.setMessage(getResources().getString(R.string.consultando));

            // Buscar dados do funcionário
            new BuscarFuncionario().execute();
        }
    }

    // Métodos de requisição

    // Buscar dados do funcionário
    public class BuscarFuncionario extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progresso.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String retorno = null;

            try {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                String token = String.valueOf(prefs.getString(TOKEN, null));

                retorno = RestUtil.get(urlFuncionario + id_funcionario, MainActivity.this, token);
            } catch (IOException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    if (erro.getMessage().contains("890")) {
                        erros = "Dados não encontrados";
                    } else {
                        erros = erro.getMessage();
                    }
                }
            }

            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (retorno != null) {
                    if (!retorno.equals("")) {
                        JSONObject fJson = new JSONObject(retorno);
                        funcionario = form.fromJson(fJson.toString(), Funcionario.class);

                        if (!funcionario.isAtivo()) {
                            if (!contemErros) {
                                contemErros = true;
                                erros = "Dados não encontrados";
                            }
                        }
                    } else {
                        if (!contemErros) {
                            contemErros = true;
                            erros = "Dados não encontrados";
                        }
                    }
                } else {
                    if (!contemErros) {
                        contemErros = true;
                        erros = "Dados não encontrados";
                    }
                }

            } catch (JSONException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //progresso.dismiss();

            if (erros == null) {
                String[] arrayNome = funcionario.getNome().trim().split(" ");
                String nome, sobrenome;

                // Deixar a primeira letra do nome e sobrenome maiúsculas
                if (arrayNome.length > 3) {
                    nome = arrayNome[0].substring(0, 1).toUpperCase() + arrayNome[0].substring(1).toLowerCase();
                    sobrenome = arrayNome[1].substring(0, 1).toUpperCase() + arrayNome[1].substring(1).toLowerCase();

                    txtNome.setText(nome + " " + sobrenome);
                } else {
                    if (arrayNome.length > 1) {
                        nome = arrayNome[0].substring(0, 1).toUpperCase() + arrayNome[0].substring(1).toLowerCase();
                        sobrenome = arrayNome[arrayNome.length - 1].substring(0, 1).toUpperCase() + arrayNome[arrayNome.length - 1].substring(1).toLowerCase();

                        txtNome.setText(nome + " " + sobrenome);
                    } else {
                        nome = arrayNome[0].substring(0, 1).toUpperCase() + arrayNome[0].substring(1).toLowerCase();
                        txtNome.setText(nome);
                    }
                }

                if (txtNome.getText().length() > 20) {
                    txtNome.setTextSize(15f);
                }

                txtEmail.setText(Html.fromHtml(getResources().getString(R.string.email_tela, "<br>" + funcionario.getEmail())));

                String cargo = funcionario.getCargo().getNome().substring(0, 1).toUpperCase() + funcionario.getCargo().getNome().substring(1).toLowerCase();
                txtCargo.setText(Html.fromHtml(getResources().getString(R.string.cargo, "<br>" + cargo)));

                Date date;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat sdf_eng = new SimpleDateFormat("MM-dd-yyyy");

                try {
                    date = sdf.parse(funcionario.getDataAdmissao());
                } catch (ParseException e) {
                    date = null;
                }

                if (Locale.getDefault().getLanguage().equals("en")) {
                    txtData.setText(Html.fromHtml(getString(R.string.admissao, "<br>" + sdf_eng.format(date))));
                } else {
                    txtData.setText(Html.fromHtml(getString(R.string.admissao, "<br>" + sdf.format(date))));
                }

                // Mudar visibilidade
                txtTitulo.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
                txtCargo.setVisibility(View.VISIBLE);
                txtEmail.setVisibility(View.VISIBLE);
                linha_1.setVisibility(View.VISIBLE);
                linha_2.setVisibility(View.VISIBLE);
                txtNome.setVisibility(View.VISIBLE);
                txtData.setVisibility(View.VISIBLE);

                new BuscarOperacao().execute();
            } else {
                tratarErro(MainActivity.this);
            }
        }
    }

    // Buscar operação do funcionário
    public class BuscarOperacao extends AsyncTask<Void, Void, String> {
        //private ProgressDialog progresso;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*progresso = new ProgressDialog(MainActivity.this);
            progresso.setTitle(getResources().getString(R.string.aguarde));
            progresso.setMessage(getResources().getString(R.string.consultando));
            progresso.show();*/
        }

        @Override
        protected String doInBackground(Void... params) {
            String retorno = null;

            try {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                String token = String.valueOf(prefs.getString(TOKEN, null));
                retorno = RestUtil.get(urlOperacao + id_funcionario + "/participa", MainActivity.this, token);
            } catch (IOException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (retorno != null && !retorno.equals("")) {
                    JSONObject oJson = new JSONObject(retorno);

                    operacao = form.fromJson(oJson.toString(), Operacao.class);
                }

            } catch (JSONException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }
            return retorno;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //progresso.dismiss();

            if (erros == null) {
                if (operacao != null) {
                    txtOperacao.setText(Html.fromHtml(getString(R.string.operacao, "<br>" + operacao.getNome())));
                    txtOperacao.setVisibility(View.VISIBLE);
                }

                new BuscarPendentes().execute();
            } else {
                tratarErro(MainActivity.this);
            }
        }
    }

    // Buscar programas pendentes para listar no menu
    public class BuscarPendentes extends AsyncTask<Void, Void, String> {
        private List<Programa> programasPendentes = new ArrayList<>(), programas = new ArrayList<>();
        private List<Aplicacao> aplicacoes = new ArrayList<>();
        //private ProgressDialog progresso;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*progresso = new ProgressDialog(MainActivity.this);
            progresso.setTitle(getResources().getString(R.string.aguarde));
            progresso.setMessage(getResources().getString(R.string.consultando));
            progresso.show();*/
        }

        @Override
        protected String doInBackground(Void... params) {
            String retorno = null;

            try {
                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                String token = String.valueOf(prefs.getString(TOKEN, null));

                retorno = RestUtil.get(urlAplicacoes + id_funcionario + "/funcionario", MainActivity.this, token);
            } catch (IOException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            try {
                GsonBuilder formatter = new GsonBuilder();
                formatter.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
                        return new Date(json.getAsJsonPrimitive().getAsLong());
                    }
                });

                Gson form = formatter.create();

                if (retorno != null && !retorno.equals("")) {
                    JSONArray jsonArray = new JSONArray(retorno);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject aplicacaoJson = jsonArray.getJSONObject(i);
                        Aplicacao aplicacao = form.fromJson(aplicacaoJson.toString(), Aplicacao.class);
                        boolean repetido = false;

                        // Adicionar Programas que o funcionário participa
                        if (aplicacao.getTreinamento() != null) {
                            if (programas.size() > 0) {
                                for (int j = 0; j < programas.size(); j++) {
                                    if (programas.get(j).getId() == aplicacao.getTreinamento().getPrograma().getId()) {
                                        repetido = true;
                                    }
                                }
                                if (!repetido) {
                                    programas.add(aplicacao.getTreinamento().getPrograma());
                                }
                            } else {
                                programas.add(aplicacao.getTreinamento().getPrograma());
                            }
                            aplicacoes.add(aplicacao);
                        }
                    }

                    // Verificar o status de cada treinamento
                    for (int i = 0; i < programas.size(); i++) {
                        int pendentes = 0;

                        Programa programa = programas.get(i);
                        for (int j = 0; j < aplicacoes.size(); j++) {
                            Aplicacao aplicacao = aplicacoes.get(j);
                            if (aplicacao.getTreinamento().getPrograma().getId() == programa.getId()) {
                                if (aplicacao.getAprovado().equals(Aprovado.PENDENTE)) {
                                    pendentes += 1;
                                }
                            }
                        }

                        // Verificar se participa do programa
                        if (verificarPrograma(programa.getId()) != null) {
                            // Verificar se concluiu o programa
                            if (pendentes != 0) {
                                programa.setAprovado(Aprovado.PENDENTE);
                                programasPendentes.add(programa);
                            }
                        }
                    }

                } else {
                    if (!contemErros) {
                        contemErros = true;
                        erros = "Dados não encontrados";
                    }
                }

            } catch (JSONException | RuntimeException erro) {
                erro.printStackTrace();
                if (!contemErros) {
                    contemErros = true;
                    erros = erro.getMessage();
                }
            }

            return retorno;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progresso.dismiss();

            if (erros == null) {
                setMenuCounter(navigationView.getMenu().getItem(2).getItemId(), programasPendentes.size());
            } else {
                tratarErro(MainActivity.this);
            }
        }
    }

    // Métodos da Classe

    // Recarregar a tela
    public void recarregarTela(View view) {
        new BuscarFuncionario().execute();
    }

    // Adaptar conteúdo à tela
    public void adaptarConteudo() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        if (larguraTela > 480 && alturaTela > 800) {
            // Diminuir tamanho da fonte
            txtTitulo.setTextSize(45);
            txtNome.setTextSize(30);

            txtEmail.setTextSize(25);
            txtEmail.setPadding(10, 10, 10, 10);

            txtCargo.setTextSize(25);
            txtCargo.setPadding(10, 10, 10, 10);

            txtData.setTextSize(25);
            txtData.setPadding(10, 10, 10, 10);

            txtOperacao.setTextSize(25f);
            txtOperacao.setPadding(10, 10, 10, 10);

            if ((larguraTela < 1600 && alturaTela < 2464) || (alturaTela < 1600 && larguraTela < 2464)) {
                // Alterar tamanho do ícone
                if (larguraTela > 800) {
                    imageView.getLayoutParams().height = 550;
                    imageView.getLayoutParams().width = 550;
                } else {
                    imageView.getLayoutParams().height = 350;
                    imageView.getLayoutParams().width = 350;
                }
            } else {
                // Aumentar tamanho da fonte
                layoutParams.setMargins(0, 45, 0, 45); // left, top, right, bottom
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
                imageView.setLayoutParams(layoutParams);

                txtTitulo.setTextSize(35);
                txtNome.setTextSize(25);
                txtEmail.setTextSize(20);
                txtCargo.setTextSize(20);
                txtData.setTextSize(20);
                txtOperacao.setTextSize(20);

                txtTitulo = aumentaTexto(txtTitulo);
                txtNome = aumentaTexto(txtNome);

                txtEmail = aumentaTexto(txtEmail);
                txtEmail.setPadding(50, 10, 50, 10);

                txtCargo = aumentaTexto(txtCargo);
                txtCargo.setPadding(50, 10, 50, 10);

                txtData = aumentaTexto(txtData);
                txtData.setPadding(50, 10, 50, 10);

                txtOperacao = aumentaTexto(txtOperacao);
                txtOperacao.setPadding(50, 10, 50, 10);

                LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParam.height = 5;
                layoutParam.setMargins(25, 30, 25, 30);
                linha_1.setLayoutParams(layoutParam);
                linha_2.setLayoutParams(layoutParam);
            }
        } else {
            txtTitulo.setTextSize(35);

            // Alterar tamanho do ícone
            imageView.getLayoutParams().height = 225;
            imageView.getLayoutParams().width = 225;
        }

    }

    // Clicar no botão de "hambúrguer" e abrir o menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Criando o menu lateral
    public void createActionBar() {
        navigationView = (NavigationView) findViewById(R.id.nav_menu);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        // Colocar botão de menu
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Mudar página ao clicar no item
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_grafico:
                        finish();
                        startActivity(new Intent(MainActivity.this, GraficoActivity.class));

                        // Apresentar animação de passagem da esquerda para direita
                        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                        return true;

                    case R.id.nav_historico:
                        finish();
                        startActivity(new Intent(MainActivity.this, HistoricoActivity.class));

                        // Apresentar animação de passagem da esquerda para direita
                        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                        return true;

                    case R.id.nav_atrasados:
                        finish();
                        startActivity(new Intent(MainActivity.this, PendentesActivity.class));

                        // Apresentar animação de passagem da esquerda para direita
                        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                        return true;

                    case R.id.nav_cores:
                        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                        View view = getLayoutInflater().inflate(R.layout.alert_theme, null);

                        alert.setView(view);

                        final TextView txtSelecione = (TextView) view.findViewById(R.id.txtSelecione);
                        final RadioButton rb_azul = (RadioButton) view.findViewById(R.id.rb_azul);
                        final RadioButton rb_roxo = (RadioButton) view.findViewById(R.id.rb_roxo);

                        // Mudar a cor da janela de acordo com o tema escolhido
                        if (buscarTema().equals("azul")) {
                            rb_azul.setChecked(true);
                            txtSelecione.setBackgroundColor(getResources().getColor(R.color.azulLogo));
                        } else {
                            rb_roxo.setChecked(true);
                            txtSelecione.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
                        }

                        // Adaptar texto em tela menor
                        adaptarTexto(txtSelecione);

                        // Adaptar texto em tela maior
                        if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
                            txtSelecione.setTextSize(20);
                            rb_azul.setTextSize(15);
                            rb_roxo.setTextSize(15);

                            txtSelecione.setTextSize(txtSelecione.getTextSize() + (txtSelecione.getTextSize() / 250));
                            rb_azul.setTextSize(rb_azul.getTextSize() + (rb_azul.getTextSize() / 250));
                            rb_roxo.setTextSize(rb_roxo.getTextSize() + (rb_roxo.getTextSize() / 250));

                            // Aumentar tamanho do ícone da janela
                            Drawable drawable = getResources().getDrawable(R.mipmap.ic_color_lens_white_24dp);
                            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            int tamanho = ((int) txtSelecione.getTextSize());
                            drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));

                            txtSelecione.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                        }

                        txtSelecione.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Medium.ttf"));
                        rb_azul.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));
                        rb_roxo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

                        // Mudar cor da janela ao clicar na cor "Azul"
                        rb_azul.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                txtSelecione.setBackgroundColor(getResources().getColor(R.color.azulLogo));
                            }
                        });

                        // Mudar a cor da janela ao clicar na cor "Roxo"
                        rb_roxo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                txtSelecione.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
                            }
                        });

                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String cor;

                                // Selecionar cor do Tema do APP
                                if (rb_azul.isChecked()) {
                                    cor = "azul";
                                } else {
                                    cor = "roxo";
                                }

                                // Salvar modificações
                                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
                                SharedPreferences.Editor editor = prefs.edit();

                                editor.putString(THEME, cor);
                                editor.commit();

                                // Recarregar a activity
                                finish();
                                startActivity(getIntent());

                                // Não mostrar animação
                                overridePendingTransition(0, 0);
                            }
                        });

                        alert.show();
                        return true;

                    case R.id.nav_logout:
                        AlertDialog.Builder alerta = new AlertDialog.Builder(MainActivity.this);

                        View dialoglayout = getLayoutInflater().inflate(R.layout.alert_exit, null);

                        alerta.setView(dialoglayout);

                        TextView txtSair = (TextView) dialoglayout.findViewById(R.id.txtSair);
                        TextView txtMensagem = (TextView) dialoglayout.findViewById(R.id.txtMensagem);

                        // Mudar a cor da janela de acordo com o tema escolhido
                        if (buscarTema().equals("azul")) {
                            txtSair.setBackgroundColor(getResources().getColor(R.color.azulLogo));
                        } else {
                            txtSair.setBackgroundColor(getResources().getColor(R.color.alternativePrincipal));
                        }

                        // Adaptar janela de sair
                        if ((larguraTela >= 1600 && alturaTela >= 2464) || (alturaTela >= 1600 && larguraTela >= 2464)) {
                            txtSair.setTextSize(20);
                            txtMensagem.setTextSize(15);

                            txtSair = aumentaTexto(txtSair);
                            txtMensagem = aumentaTexto(txtMensagem);

                            // Aumentar tamanho do ícone da janela
                            Drawable drawable = getResources().getDrawable(R.mipmap.ic_warning_white_24dp);
                            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            int tamanho = ((int) txtSair.getTextSize());
                            drawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, tamanho, tamanho, true));

                            txtSair.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                        }

                        txtSair.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Medium.ttf"));
                        txtMensagem.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HindVadodara-Regular.ttf"));

                        alerta.setPositiveButton(getResources().getString(R.string.sim), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Validar o logout
                                Intent resultado = new Intent(MainActivity.this, LoginActivity.class);
                                resultado.putExtra("saiu", true);
                                startActivity(resultado);

                                SharedPreferences prefs = getSharedPreferences(APP_PREFS, MODE_PRIVATE);

                                // Voltar ao tema padrão
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString(THEME, "azul");
                                editor.putString(EMAIL, null);
                                editor.putString(SENHA, null);
                                editor.commit();

                                dialog.dismiss();
                                finish();

                                // Apresentar animação de passagem da direita para esquerda
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                            }
                        });

                        alerta.setNegativeButton(getResources().getString(R.string.nao), null);

                        alerta.show();

                        return true;
                    default:
                        return true;
                }
            }
        });

    }

    // Colocar o número de programas pendentes no menu
    protected void setMenuCounter(@IdRes int itemId, int count) {
        TextView view = (TextView) navigationView.getMenu().findItem(itemId).getActionView();

        view.setText(count > 0 ? String.valueOf(count) : null);
        view.setBackgroundResource(R.drawable.notificacao);
        view.setGravity(Gravity.CENTER_VERTICAL);

        if (count != 0) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    // Requisitar permissão de internet
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for (int resultado : grantResults) {
            if (resultado == PackageManager.PERMISSION_DENIED) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                builder.setTitle("Sem conceder a permissão app não funcionará");
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }

    // Quando voltar à tela inicial
    @Override
    protected void onResume() {
        super.onResume();

        Bundle bundle = getIntent().getExtras();

        if (bundle == null) {
            // Buscar dados do funcionário
            new BuscarFuncionario().execute();
        }
    }

    // Finalizar a activity quando clicar em voltar do celular
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Minimizar a tela
        moveTaskToBack(true);
    }
}