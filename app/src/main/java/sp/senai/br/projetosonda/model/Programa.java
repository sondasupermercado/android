package sp.senai.br.projetosonda.model;

import java.util.Set;

/**
 * Created by Sonda on 31/03/2017.
 */

public class Programa {
    private Long id;
    private String nome;
    private int duracao;
    private int prazoReciclagem;
    private int prazoNotificacao;
    private int prazoEficacia;
    private Operacao operacao;
    private Cargo cargo;
    private Set<Treinamento> treinamentos;
    private boolean ativo = true;
    private Funcionario coordenador;
    private Set<Funcionario> funcionarios;
    private String dataInsercao;
    private Aprovado aprovado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public int getPrazoReciclagem() {
        return prazoReciclagem;
    }

    public void setPrazoReciclagem(int prazoReciclagem) {
        this.prazoReciclagem = prazoReciclagem;
    }

    public int getPrazoNotificacao() {
        return prazoNotificacao;
    }

    public void setPrazoNotificacao(int prazoNotificacao) {
        this.prazoNotificacao = prazoNotificacao;
    }

    public int getPrazoEficacia() {
        return prazoEficacia;
    }

    public void setPrazoEficacia(int prazoEficacia) {
        this.prazoEficacia = prazoEficacia;
    }

    public Operacao getOperacao() {
        return operacao;
    }

    public void setOperacao(Operacao operacao) {
        this.operacao = operacao;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Set<Treinamento> getTreinamentos() {
        return treinamentos;
    }

    public void setTreinamentos(Set<Treinamento> treinamentos) {
        this.treinamentos = treinamentos;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Funcionario getCoordenador() {
        return coordenador;
    }

    public void setCoordenador(Funcionario coordenador) {
        this.coordenador = coordenador;
    }

    public Set<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(Set<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public String getDataInsercao() {
        return dataInsercao;
    }

    public void setDataInsercao(String dataInsercao) {
        this.dataInsercao = dataInsercao;
    }

    public Aprovado getAprovado() {
        return aprovado;
    }

    public void setAprovado(Aprovado aprovado) {
        this.aprovado = aprovado;
    }

    @Override
    public String toString() {
        return "Programa{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", duracao=" + duracao +
                '}';
    }
}
