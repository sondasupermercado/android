package sp.senai.br.projetosonda.model;

import java.util.List;

/**
 * Created by Sonda on 03/04/2017.
 */

public class Treinamento {
    private Long id;
    private Programa programa;
    private Conteudo conteudo;
    private List<Turma> turmas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Conteudo getConteudo() {
        return conteudo;
    }

    public void setConteudo(Conteudo conteudo) {
        this.conteudo = conteudo;
    }

    public List<Turma> getTurmas() {
        return turmas;
    }

    public void setTurmas(List<Turma> turmas) {
        this.turmas = turmas;
    }

    @Override
    public String toString() {
        return "Treinamento{" +
                "programa=" + programa +
                '}';
    }
}
